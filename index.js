
var characterList = document.getElementById("CharacterList");


const url = 'https://rickandmortyapi.com/api/character';
fetch(url)
    .then(res => res.json())
    .then(data => {
        const rawData = data.results;
        return rawData.map((character, index) => {
          //all needed data is listed below as an entity 
           let created = character.created;
           let species = character.species ;
           let img = character.image;
           let episodes = character.episode;
           let name = character.name;
           let location = character.location;
            //create element
           

            var characterCard = document.createElement('div');
            characterCard.id = index;
            characterCard.className = "card";


            var cardImg = document.createElement('img');
            cardImg.className = "card-img-top";
            cardImg.src = img;
            cardImg.alt = "Card image cap";
            characterCard.appendChild(cardImg);

            var cardBody = document.createElement('div');
            cardBody.className = "card-body";
            characterCard.appendChild(cardBody);

            var cardTitle = document.createElement('h5');
            cardTitle.className = "card-title";
            cardTitle.innerHTML = name;
            cardBody.appendChild(cardTitle);
            
            var cardList = document.createElement('ul');
            cardList.className = "list-group list-group-flush";
            characterCard.appendChild(cardList);

            var listSpecies = document.createElement('li');
            listSpecies.className = "list-group-item";
            listSpecies.innerHTML = "Species: " + species;
            cardList.appendChild(listSpecies);

            var listLocation = document.createElement('li');
            listLocation.className = "list-group-item";
            listLocation.innerHTML = "Last location: " + location.name;
            cardList.appendChild(listLocation);

            var listEpisodes = document.createElement('li');
            listEpisodes.className = "list-group-item";
            listEpisodes.innerHTML = "Number of episodes: " + episodes.length;
            cardList.appendChild(listEpisodes);

            var listCreated = document.createElement('li');
            listCreated.className = "list-group-item";
            listCreated.innerHTML = "Date of creation: " + created;
            cardList.appendChild(listCreated);


            //append element
            characterList.appendChild(characterCard);
            
        });

    })
    .catch((error) => {
        console.log(JSON.stringify(error));
    });
